#!/bin/bash

start_path=$2
program=$1

if [ "$#" -ne 2 ]; then
	echo "Illegal number of parameters."
	exit 1
fi

if [ ! -f "$program" -o ! -x "$program" ]; then
	echo "Program $program is missing."
	exit 1
fi

if [ ! -d "$start_path" ]; then
	echo "Directory $start_path is missing."
	exit 1
fi

start_found=0
OIFS="$IFS"
IFS=$'\n'
for file in `find $2 -type f`
do
	line=$(head -n 1 "$file")
	if [ "$line" = "START" ]; then
		start_found=1
		start_file=$file
	fi
done
IFS="$OIFS"

if [ $start_found -eq 1 ]; then
	temp=$(mktemp)
	$1 <"$start_file" 2>/dev/null >"$temp"
fi

foundstop=0
lastline=$(tail -1 "$start_file")
if [ "$lastline" = "STOP" ]; then
	foundstop=1
fi

file_path=$start_file
while [ $foundstop -ne 1 ]
do 
	file_path=$(grep FILE $file_path)
	file_path=${file_path%/*}
	file_path_found=$?
	if [ $file_path_found -eq 0 ]; then
		file_path=${file_path/"FILE "/""}
		file_path="$start_path/$file_path"
		prevtemp=$temp
		temp=$(mktemp)
		cat <"$file_path" >>"$prevtemp"
		$program <"$prevtemp" 2>/dev/null >"$temp"
	fi
	lastline=$(tail -1 "$file_path")
	if [ "$lastline" = "STOP" ]; then
		foundstop=1
	fi
done

if [ !  -z "$(<"$temp")" ]; then
	echo "$(<"$temp")"
fi
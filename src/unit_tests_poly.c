/*
 * Copyright 2008 Google Inc.
 * Copyright 2015 Tomasz Kociumaka
 * Copyright 2016, 2017 IPP team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** @file
	Testy jednostkowe funkcji PolyCompose i parsera dla komendy COMPOSE.
	Atrapy funkcji z biblioteki standardowej są skopiowane z zewnątrz i dotyczy ich komentarz na początku pliku.
	@author Michał Junik <mj385655@students.mimuw.edu.pl>
	@date 2017-06-21
*/

#include <stdio.h>
#include <stdarg.h>
#include <setjmp.h>
#include <stdlib.h>
#include <string.h>
#include "cmocka.h"

#include "poly.h"

#define array_length(x) (sizeof(x) / sizeof((x)[0]))///<makro do obliczania długości tablicy

static jmp_buf jmp_at_exit;///<...
static int exit_status;///<wartość zwrócona przez main


static char *argv = NULL;///<argument dla funkcji main
static int argc = 0;///<argument dla funkcji main

extern int calculator_main(int argc, char *argv[]);///<funkcja main z calc_poly.c

/**
 * Atrapa funkcji main
 */
int mock_main(int argc, char *argv[]) {
	if (!setjmp(jmp_at_exit))
		return calculator_main(argc, argv);
	return exit_status;
}

/**
 * Atrapa funkcji exit
 */
void mock_exit(int status) {
	exit_status = status;
	longjmp(jmp_at_exit, 1);
}

int mock_fprintf(FILE* const file, const char *format, ...) CMOCKA_PRINTF_ATTRIBUTE(2, 3);
int mock_printf(const char *format, ...) CMOCKA_PRINTF_ATTRIBUTE(1, 2);

/*
 * Pomocnicze bufory, do których piszą atrapy funkcji printf i fprintf oraz
 * pozycje zapisu w tych buforach. Pozycja zapisu wskazuje bajt o wartości 0.
 */
static char fprintf_buffer[256];///<Pomocniczy bufor dla atrap symulujący stderr
static char printf_buffer[256];///<Pomocniczy bufor dla atrap symulujący stdout
static int fprintf_position = 0;///<Pozycja w buforze stderr
static int printf_position = 0;///<Pozycja w buforze stdout

/**
 * Atrapa funkcji fprintf sprawdzająca poprawność wypisywania na stderr.
 */
int mock_fprintf(FILE* const file, const char *format, ...) {
	int return_value;
	va_list args;

	assert_true(file == stderr);
	/* Poniższa asercja sprawdza też, czy fprintf_position jest nieujemne.
	W buforze musi zmieścić się kończący bajt o wartości 0. */
	assert_true((size_t)fprintf_position < sizeof(fprintf_buffer));

	va_start(args, format);
	return_value = vsnprintf(fprintf_buffer + fprintf_position,
							 sizeof(fprintf_buffer) - fprintf_position,
							 format,
							 args);
	va_end(args);

	fprintf_position += return_value;
	assert_true((size_t)fprintf_position < sizeof(fprintf_buffer));
	return return_value;
}

/**
 * Atrapa funkcji fprintf sprawdzająca poprawność wypisywania na stdout.
 */
int mock_printf(const char *format, ...) {
	int return_value;
	va_list args;

	/* Poniższa asercja sprawdza też, czy printf_position jest nieujemne.
	W buforze musi zmieścić się kończący bajt o wartości 0. */
	assert_true((size_t)printf_position < sizeof(printf_buffer));

	va_start(args, format);
	return_value = vsnprintf(printf_buffer + printf_position,
							 sizeof(printf_buffer) - printf_position,
							 format,
							 args);
	va_end(args);

	printf_position += return_value;
	assert_true((size_t)printf_position < sizeof(printf_buffer));
	return return_value;
}

/**
 *  Pomocniczy bufor, z którego korzystają atrapy funkcji operujących na stdin.
 */
static char input_stream_buffer[256];
static int input_stream_position = 0;///<Pozycja w pomocniczym buforze symulującym stdin
static int input_stream_end = 0;///<Indeks ostatnie pozycji w byforze symulującym stdin
int read_char_count;///<Ilość charów przeczytana z bufora symulującego stdin

/**
 * Atrapa funkcji scanf używana do przechwycenia czytania z stdin.
 */
int mock_scanf(const char *format, ...) {
	va_list fmt_args;
	int ret;

	va_start(fmt_args, format);
	ret = vsscanf(input_stream_buffer + input_stream_position, format, fmt_args);
	va_end(fmt_args);

	if (ret < 0) { /* ret == EOF */
		input_stream_position = input_stream_end;
	}
	else {
		assert_true(read_char_count >= 0);
		input_stream_position += read_char_count;
		if (input_stream_position > input_stream_end) {
			input_stream_position = input_stream_end;
		}
	}
	return ret;
}

/**
 * Atrapa funkcji getchar używana do przechwycenia czytania z stdin.
 */
int mock_getchar() {
	if (input_stream_position < input_stream_end)
		return input_stream_buffer[input_stream_position++];
	else
		return EOF;
}

/**
 * Atrapa funkcji ungetc.
 * Obsługiwane jest tylko standardowe wejście.
 */
int mock_ungetc(int c, FILE *stream) {
	assert_true(stream == stdin);
	if (input_stream_position > 0)
		return input_stream_buffer[--input_stream_position] = c;
	else
		return EOF;
}

/* 
 * END OF LICENSED PART
 */

/**
 * \defgroup unit_tests_compose_group Testy jednostkowe PolyCompose.
 * Testy jednostkowe przypadków granicznych dla funkcji PolyCompose.
 * @{
 */

/**
 * Test funkcji PolyCompose dla p = 0 i count = 0.
 * @param[in] state : zmienna zawierająca stan
 */
static void test_compose_p_zero_count_zero(void **state){
	(void)state;
	Poly zero = PolyZero();

	Poly p_res = PolyCompose(&zero, 0, NULL);
	Poly p_exp_res = PolyZero();
	assert_true(PolyIsEq(&p_res,&p_exp_res));

	PolyDestroy(&zero);
	PolyDestroy(&p_res);
	PolyDestroy(&p_exp_res);
}

/**
 * Test funkcji PolyCompose dla p = 0, count = 1 i x = const.
 * @param[in] state : zmienna zawierająca stan
 */
static void test_compose_p_zero_count_one_x_const(void **state){
	(void)state;
	Poly zero = PolyZero();
	Poly x = PolyFromCoeff(4);

	Poly p_res = PolyCompose(&zero, 1, &x);
	Poly p_exp_res = PolyZero();
	assert_true(PolyIsEq(&p_res,&p_exp_res));
	
	PolyDestroy(&zero);
	PolyDestroy(&x);
	PolyDestroy(&p_res);
	PolyDestroy(&p_exp_res);
}

/**
 * Test funkcji PolyCompose dla p = const i count = 0.
 * @param[in] state : zmienna zawierająca stan
 */
static void test_compose_p_const_count_zero(void **state){
	(void)state;
	Poly poly = PolyFromCoeff(4);

	Poly p_res = PolyCompose(&poly, 0, NULL);
	Poly p_exp_res = PolyFromCoeff(4);
	assert_true(PolyIsEq(&p_res,&p_exp_res));
	
	PolyDestroy(&poly);
	PolyDestroy(&p_res);
	PolyDestroy(&p_exp_res);
}

/**
 * Test funkcji PolyCompose dla p = const, count = 1 i x = const.
 * @param[in] state : zmienna zawierająca stan
 */
static void test_compose_p_const_count_one_x_const(void **state){
	(void)state;
	Poly poly = PolyFromCoeff(4);
	Poly x = PolyFromCoeff(9);

	Poly p_res = PolyCompose(&poly, 1, &x);
	Poly p_exp_res = PolyFromCoeff(4);
	assert_true(PolyIsEq(&p_res,&p_exp_res));
	
	PolyDestroy(&poly);
	PolyDestroy(&x);
	PolyDestroy(&p_res);
	PolyDestroy(&p_exp_res);
}

/**
 * Test funkcji PolyCompose dla p = x_0 i count = 0.
 * @param[in] state : zmienna zawierająca stan
 */
static void test_compose_p_var_count_zero(void **state){
	(void)state;
	Poly one = PolyFromCoeff(1);
	Mono mono = MonoFromPoly(&one, 1);
	Poly poly = PolyAddMonos(1, &mono);

	Poly p_res = PolyCompose(&poly, 0, NULL);
	Poly p_exp_res = PolyZero();
	assert_true(PolyIsEq(&p_res,&p_exp_res));
	
	PolyDestroy(&one);
	MonoDestroy(&mono);
	PolyDestroy(&poly);
	PolyDestroy(&p_res);
	PolyDestroy(&p_exp_res);
}

/**
 * Test funkcji PolyCompose dla p = x_0, count = 1 i x = const.
 * @param[in] state : zmienna zawierająca stan
 */
static void test_compose_p_var_count_one_x_const(void **state){
	(void)state;
	Poly one = PolyFromCoeff(1);
	Mono mono = MonoFromPoly(&one, 1);
	Poly poly = PolyAddMonos(1, &mono);
	Poly x = PolyFromCoeff(9);

	Poly p_res = PolyCompose(&poly, 1, &x);
	Poly p_exp_res = PolyFromCoeff(9);
	assert_true(PolyIsEq(&p_res,&p_exp_res));
	
	PolyDestroy(&one);
	MonoDestroy(&mono);
	PolyDestroy(&poly);
	PolyDestroy(&x);
	PolyDestroy(&p_res);
	PolyDestroy(&p_exp_res);
}

/**
 * Test funkcji PolyCompose dla p = x_0, count = 1 i x = x_0.
 * @param[in] state : zmienna zawierająca stan
 */
static void test_compose_p_var_count_one_x_var(void **state){
	(void)state;
	Poly one = PolyFromCoeff(1);
	Mono mono = MonoFromPoly(&one, 1);
	Poly poly = PolyAddMonos(1, &mono);

	Poly x_one = PolyFromCoeff(1);
	Mono x_mono = MonoFromPoly(&x_one, 1);
	Poly x = PolyAddMonos(1, &x_mono);

	Poly p_res = PolyCompose(&poly, 1, &x);
	Poly exp_res_one = PolyFromCoeff(1);
	Mono exp_res_mono = MonoFromPoly(&exp_res_one, 1);
	Poly p_exp_res = PolyAddMonos(1, &exp_res_mono);
	assert_true(PolyIsEq(&p_res,&p_exp_res));
	
	PolyDestroy(&one);
	MonoDestroy(&mono);
	PolyDestroy(&poly);
	PolyDestroy(&x_one);
	MonoDestroy(&x_mono);
	PolyDestroy(&x);
	PolyDestroy(&p_res);
	PolyDestroy(&exp_res_one);
	MonoDestroy(&exp_res_mono);
	PolyDestroy(&p_exp_res);
}

/**
 * @}
 */

/**
 * Inicjuje wartość bufora symulującego wejście standardowe.
 * @param[in] str : tekst który ma być dodany do strumienia
 */
static void init_input_stream(const char *str) {
	memset(input_stream_buffer, 0, sizeof(input_stream_buffer));
	input_stream_position = 0;
	input_stream_end = strlen(str);
	assert_true((size_t)input_stream_end < sizeof(input_stream_buffer));
	strcpy(input_stream_buffer, str);
}

/**
 * \defgroup unit_tests_parser_group Testy jednostkowe parsera.
 * Testy jednostkowe przypadków granicznych dla parsera komendy COMPOSE.
 * @{
 */

/**
 * Test parsera dla COMPOSE bez parametrów.
 * @param[in] state : zmienna zawierająca stan
 */
static void test_parser_param_none(void **state) {
	(void)state;
	init_input_stream("0\nCOMPOSE");

	assert_int_equal(mock_main(argc, (char **)argv), 0);
	assert_string_equal(fprintf_buffer, "ERROR 2 WRONG COUNT\n");
	assert_string_equal(printf_buffer, "");
}

/**
 * Test parsera dla COMPOSE 0.
 * @param[in] state : zmienna zawierająca stan
 */
static void test_parser_param_zero(void **state) {
	(void)state;
	init_input_stream("0\nCOMPOSE 0");

	assert_int_equal(mock_main(argc, (char **)argv), 0);
	assert_string_equal(fprintf_buffer, "");
	assert_string_equal(printf_buffer, "");
}

/**
 * Test parsera dla COMPOSE UINT_MAX.
 * @param[in] state : zmienna zawierająca stan
 */
static void test_parser_param_uint_max(void **state) {
	(void)state;
	init_input_stream("0\nCOMPOSE 4294967295");

	assert_int_equal(mock_main(argc, (char **)argv), 0);
	assert_string_equal(fprintf_buffer, "ERROR 2 STACK UNDERFLOW\n");
	assert_string_equal(printf_buffer, "");
}

/**
 * Test parsera dla COMPOSE -1.
 * @param[in] state : zmienna zawierająca stan
 */
static void test_parser_param_minus_one(void **state) {
	(void)state;
	init_input_stream("0\nCOMPOSE -1");

	assert_int_equal(mock_main(argc, (char **)argv), 0);
	assert_string_equal(fprintf_buffer, "ERROR 2 WRONG COUNT\n");
	assert_string_equal(printf_buffer, "");
}

/**
 * Test parsera dla COMPOSE UINT_MAX+1.
 * @param[in] state : zmienna zawierająca stan
 */
static void test_parser_param_uint_max_plus_one(void **state) {
	(void)state;
	init_input_stream("0\nCOMPOSE 4294967296");

	assert_int_equal(mock_main(argc, (char **)argv), 0);
	assert_string_equal(fprintf_buffer, "ERROR 2 WRONG COUNT\n");
	assert_string_equal(printf_buffer, "");
}

/**
 * Test parsera dla COMPOSE dla dużej liczby, większej od UINT_MAX.
 * @param[in] state : zmienna zawierająca stan
 */
static void test_parser_param_big_number(void **state) {
	(void)state;
	init_input_stream("0\nCOMPOSE 1284294967296");

	assert_int_equal(mock_main(argc, (char **)argv), 0);
	assert_string_equal(fprintf_buffer, "ERROR 2 WRONG COUNT\n");
	assert_string_equal(printf_buffer, "");
}

/**
 * Test parsera dla COMPOSE dla kombinacji liter.
 * @param[in] state : zmienna zawierająca stan
 */
static void test_parser_param_letters(void **state) {
	(void)state;
	init_input_stream("0\nCOMPOSE rayobts");

	assert_int_equal(mock_main(argc, (char **)argv), 0);
	assert_string_equal(fprintf_buffer, "ERROR 2 WRONG COUNT\n");
	assert_string_equal(printf_buffer, "");
}

/**
 * Test parsera dla COMPOSE dla kombinacji cyfr i liter zaczynającej się cyfrą.
 * @param[in] state : zmienna zawierająca stan
 */
static void test_parser_param_digits_and_letters(void **state) {
	(void)state;
	init_input_stream("0\nCOMPOSE 32a19x9");

	assert_int_equal(mock_main(argc, (char **)argv), 0);
	assert_string_equal(fprintf_buffer, "ERROR 2 WRONG COUNT\n");
	assert_string_equal(printf_buffer, "");
}

/**
 * @}
 */

/**
 * Funkcja pomocnicza wywoływana przed każdym wykonaniem testu parsowania.
 * @param[in] state : zmienna zawierająca stan
 * @return int 0 jeśli operacja się powiodła.
 */
static int test_io_setup(void **state) {
	(void)state;
	memset(fprintf_buffer, 0, sizeof(fprintf_buffer));
	memset(printf_buffer, 0, sizeof(printf_buffer));
	printf_position = 0;
	fprintf_position = 0;

	return 0;
}

/**
 * Funkcja pomocnicza wywoływana raz przed grupą testów
 * @param[in] state : zmienna zawierająca stan
 * @return int 0 jeśli operacja się powiodła.
 */
static int test_setup(void **state) {
	(void)state;
	return 0;
}

/**
 * Funkcja pomocnicza wywoływana raz po grupie testów
 * @param[in] state : zmienna zawierająca stan
 * @return int 0 jeśli operacja się powiodła.
 */
static int test_teardown(void **state) {
	(void)state;
	// free(*state);
	return 0;
}

/**
 * main
 * @returns int
 */
int main() {
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(test_compose_p_zero_count_zero),
		cmocka_unit_test(test_compose_p_zero_count_one_x_const),
		cmocka_unit_test(test_compose_p_const_count_zero),
		cmocka_unit_test(test_compose_p_const_count_one_x_const),
		cmocka_unit_test(test_compose_p_var_count_zero),
		cmocka_unit_test(test_compose_p_var_count_one_x_const),
		cmocka_unit_test(test_compose_p_var_count_one_x_var)
		//cmocka_unit_test_setup_teardown(test_parser_example, test_io_setup, test_io_teardown)
	};

	const struct CMUnitTest parse_tests[] = {
		cmocka_unit_test_setup(test_parser_param_none, test_io_setup),
		cmocka_unit_test_setup(test_parser_param_zero, test_io_setup),
		cmocka_unit_test_setup(test_parser_param_uint_max, test_io_setup),
		cmocka_unit_test_setup(test_parser_param_minus_one, test_io_setup),
		cmocka_unit_test_setup(test_parser_param_uint_max_plus_one, test_io_setup),
		cmocka_unit_test_setup(test_parser_param_big_number, test_io_setup),
		cmocka_unit_test_setup(test_parser_param_letters, test_io_setup),
		cmocka_unit_test_setup(test_parser_param_digits_and_letters, test_io_setup)
	};

	return cmocka_run_group_tests(tests, test_setup, test_teardown) + 
	cmocka_run_group_tests(parse_tests, test_setup, test_teardown); 
}
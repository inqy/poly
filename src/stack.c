/** @file
   Implementacja stosu wielomianów.

   @author Michał Junik <mj385655@students.mimuw.edu.pl>
   @date 2017-05-22
*/
#include <assert.h>
#include <stdlib.h>

#include "stack.h"

void StackPush(PolyStack* s, Poly p) {
	PolyStackElement* ptr = malloc(sizeof(PolyStackElement));
	ptr->p = p;
	ptr->next = s->head;

	s->head = ptr;
	s->size++;
}

Poly StackPop(PolyStack* s) {
	assert(s->size > 0);
	PolyStackElement* tmp = s->head;
	s->head = tmp->next;
	s->size--;

	Poly p = tmp->p;
	free(tmp);

	return p;
}

Poly StackPeek(PolyStack* s) {
	assert(s->size > 0);
	return s->head->p;

}

void StackDestroy(PolyStack* s) {
	while(!StackIsEmpty(s)) {
		Poly p = StackPop(s);
		PolyDestroy(&p);
	}
}